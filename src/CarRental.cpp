/*
 * CarRental.cpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#include "CarRental.h"

CarRental::CarRental() {
}
CarRental::CarRental(std::string phoneNumber, std::string eMail) :
		phoneNumber(phoneNumber), eMail(eMail) {
}
CarRental::~CarRental() {
	// TODO Auto-generated destructor stub
}

bool CarRental::addCar(Car::brand brand_, Car::model model_,
		std::string registrationNumber_, double priceForDay_,
		int productionYear_, bool isAvailable_) {
	Car car(brand_, model_, registrationNumber_, priceForDay_, productionYear_);
	if (isRegistrationUnique(&car) == true) {
		cars[car.getRegistrationNumber()] = car;
		return true;
	} else
		return false;
}
bool CarRental::isRegistrationUnique(Car *car) const{
	for (std::map<std::string, Car>::const_iterator it = cars.begin(); it != cars.end(); ++it)
		if (it->first == car->getRegistrationNumber())
			return false;
	return true;
}

bool CarRental::addCar(Car car) {
	if (isRegistrationUnique(&car) == true) {
		cars[car.getRegistrationNumber()] = car;
		return true;
	} else
		return false;
}

void CarRental::deleteCar(Car car) {
	for (std::map<std::string, Car>::iterator it = cars.begin(); it != cars.end(); ++it) {
		if (it->first == car.getRegistrationNumber())
			cars.erase(it);
	}
}
bool CarRental::rentCar(std::string registrationNumber) {
	if (cars[registrationNumber].isIsAvailable()) {
		cars[registrationNumber].setIsAvailable(false);
		return true;
	} else
		return false;
}

std::vector<Car*> CarRental::findCarByProductionYear(int since, int to){
	std::vector<Car*> preferredCars;
	for (std::map<std::string, Car>::iterator it = cars.begin(); it != cars.end(); ++it) {
		if (it->second.getProductionYear() >= since && it->second.getProductionYear() <= to)
			preferredCars.push_back(&(it->second));
	}
	return preferredCars;
}
