/*
 * Menu.h
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef MENU_H_
#define MENU_H_

#include "BaseInterface.h"
#include "CarRental.h"
#include <vector>

class Menu {
public:
	Menu();
	Menu(CarRental *carRental);
	virtual ~Menu();

	void runSystem();
	void showMenu() const;
	int chooseOption() const;
	void rentObject();
	void printAllCars() const;
	void performTask(int task);
	int chooseCar();
	std::string enterRegistrationNumber();
	void enterToContinue();
	bool returnCar();
	void addNewCar();
	std::string enterBrandCar();
	int addRentedDays();
	double priceToPayForRent(int carNumber);
	void printCarRenthalContact() const;
	std::vector<Car*> findPrefferedCars();
	void printPreferredCars(std::vector<Car*> preferredCars) const;
private:
	CarRental *carRental;

	int getYear();

};

#endif /* MENU_H_ */
