
//#include "gtest/gtest.h"
//#include "Car.h"
//#include "CarRental.h"
//#include "Menu.h"
//
//class CarRentalOperationTestFixture : public testing::Test {
//protected:
//	Car testCar1;
//	Car testCar2;
//	Car testCar3;
//	Car testCar4;
//	Car testCar5;
//	CarRental testCarRental;
//
//	void SetUp() {
//		testCar1=Car(Car::Honda, Car::Accord, "KR124", 35.50, 2016, 1);
//		testCar2=Car(Car::Opel, Car::Astra, "KR126", 23.00, 2016, 1);
//		testCar3=Car(Car::Fiat, Car::Panda, "KR127", 22.50, 2017, 1);
//		testCar4=Car(Car::Fiat, Car::Punto, "KR125", 20.50, 2017, 0);
//		testCar5=Car(Car::Toyota, Car::Corolla, "KR210", 22.50, 2015, 0);
//		testCarRental.addCar(testCar1);
//		testCarRental.addCar(testCar2);
//		testCarRental.addCar(testCar3);
//	}
//	virtual void TearDown() {
//	}
//};
//
////Class Car
//
//TEST_F (CarRentalOperationTestFixture, CarContructionCalledCorrectly){
//	EXPECT_EQ(3,testCar1.getCarBrand());
//	EXPECT_EQ(7,testCar1.getCarModel());
//	EXPECT_EQ("KR124",testCar1.getRegistrationNumber());
//	EXPECT_EQ(35.50,testCar1.getPriceForDay());
//	EXPECT_EQ(2016,testCar1.getProductionYear());
//	EXPECT_EQ(1, testCar1.isIsAvailable());
//}
//
//TEST_F (CarRentalOperationTestFixture, ReturnBrandCalledCorrectly){
//	EXPECT_EQ(Car::Toyota,testCar5.returnBrand("Toyota"));
//	EXPECT_EQ(Car::Honda,testCar1.returnBrand("Honda"));
//	EXPECT_EQ(Car::Opel,testCar2.returnBrand("Opel"));
//	EXPECT_EQ(Car::Fiat,testCar3.returnBrand("Fiat"));
//}
//
//TEST_F (CarRentalOperationTestFixture, ReturnModelCalledCorrectly){
//	EXPECT_EQ(Car::Accord,testCar1.returnModel("Accord"));
//	EXPECT_EQ(Car::Astra,testCar2.returnModel("Astra"));
//	EXPECT_EQ(Car::Panda,testCar3.returnModel("Panda"));
//}
//
//// CarRentalClass
//
//TEST_F (CarRentalOperationTestFixture, AddCarCalledCorrectly){
//	EXPECT_FALSE(testCarRental.addCar(testCar1));
//	EXPECT_FALSE(testCarRental.addCar(testCar2));
//	EXPECT_FALSE(testCarRental.addCar(testCar3));
//	EXPECT_TRUE(testCarRental.addCar(testCar4));
//	EXPECT_TRUE(testCarRental.addCar(testCar5));
//}
//
//TEST_F (CarRentalOperationTestFixture, IsRegistrationUniqueCalledCorrectly){
//	EXPECT_FALSE(testCarRental.isRegistrationUnique(&testCar1));
//	EXPECT_FALSE(testCarRental.isRegistrationUnique(&testCar2));
//	EXPECT_FALSE(testCarRental.isRegistrationUnique(&testCar3));
//	Car testCar4(Car::Honda, Car::Accord, "KR987", 35.50, 2016, 1);
//	EXPECT_TRUE(testCarRental.isRegistrationUnique(&testCar4));
//}
//
//TEST_F (CarRentalOperationTestFixture, RentCarCalledCorrectly){
//	EXPECT_TRUE(testCarRental.addCar(testCar4));
//	EXPECT_TRUE(testCarRental.addCar(testCar5));
//	EXPECT_TRUE(testCarRental.rentCar(1));
//	EXPECT_TRUE(testCarRental.rentCar(2));
//	EXPECT_TRUE(testCarRental.rentCar(3));
//	EXPECT_FALSE(testCarRental.rentCar(4));
//	EXPECT_FALSE(testCarRental.rentCar(5));
//}
//TEST_F (CarRentalOperationTestFixture, PreferredCarsSelectedCorrectly){
//	std::vector<Car*> preferredCars = testCarRental.findCarByProductionYear(2017, 2017);
//	EXPECT_EQ(1u, preferredCars.size());
//	EXPECT_EQ(testCar3.getCarBrand(), preferredCars[0]->getCarBrand());
//	EXPECT_EQ(testCar3.getCarModel(), preferredCars[0]->getCarModel());
//	EXPECT_EQ(testCar3.getRegistrationNumber(), preferredCars[0]->getRegistrationNumber());
//}

