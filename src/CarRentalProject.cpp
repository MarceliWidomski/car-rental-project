//============================================================================
// Name        : CarRentalProject.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


#include <iostream>
#include "Car.h"
#include "BaseInterface.h"
#include "Menu.h"
#include "gtest/gtest.h"



int main(int argc, char **argv) {

	//TODO add new car

//	::testing::InitGoogleTest(&argc, argv);
//	return RUN_ALL_TESTS();

	Car ToyotaYaris(Car::Toyota, Car::Yaris, "KR12345", 2016, 100);
	Car ToyotaYaris2(Car::Toyota, Car::Yaris, "KR98765", 2015, 100, 0);
	Car HondaAccord(Car::Honda, Car::Accord, "KR54356", 2017, 120);
	Car HondaCivic(Car::Honda, Car::Civic, "KR98555", 2017, 150);

	CarRental carRental("790 111 111", "MMCarRenthal@gmail.com");
	carRental.addCar(ToyotaYaris);
	carRental.addCar(ToyotaYaris2);
	carRental.addCar(HondaAccord);
	carRental.addCar(HondaCivic);

	Menu menu(&carRental);
	menu.runSystem();

	return 0;
}
