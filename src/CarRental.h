/*
 * CarRental.h
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef CARRENTAL_H_
#define CARRENTAL_H_

#include "Car.h"
#include <map>
#include <vector>
#include <iostream>
#include <string>

class CarRental {
public:
	CarRental();
	CarRental(std::string phoneNumber, std::string eMail);
	virtual ~CarRental();

	bool addCar(Car car);
	bool addCar(Car::brand brand_, Car::model model_, std::string registrationNumber_,
			double priceForDay_, int productionYear_, bool isAvailable_);
	void deleteCar(Car car);
	bool rentCar(std::string registrationNumber);
	void collectCar(Car car);
//	const std::vector<Car>& getCars() const {return cars;}
	bool isRegistrationUnique(Car *car) const;
	const std::string& getMail() const {return eMail;}
	void setMail(const std::string& mail) {eMail = mail;}
	const std::string& getPhoneNumber() const {return phoneNumber;}
	void setPhoneNumber(const std::string& phoneNumber) {this->phoneNumber = phoneNumber;}
	std::vector<Car*> findCarByProductionYear(int since, int to);

private:
	std::map<std::string, Car> cars;
	std::string phoneNumber;
	std::string eMail;

};

#endif /* CARRENTAL_H_ */
