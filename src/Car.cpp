/*
 * Car.cpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#include "Car.h"
#include <iostream>

Car::Car() {
	this->carBrand = NoBrand;
	this->carModel = NoModel;
	this->priceForDay = 0;
	this->productionYear = 0;
	this->isAvailable = true;
	this->rentedDays = 0;
}

Car::Car(Car::brand brand_, Car::model model_, std::string registrationNumber_,
		int productionYear_, double priceForDay_, bool isAvailable_) :
		carBrand(brand_), carModel(model_), registrationNumber(
				registrationNumber_), productionYear(productionYear_), priceForDay(
				priceForDay_), isAvailable(isAvailable_) {
	rentedDays = 0;
}
Car::~Car() {
}
void Car::printCar() const {
	std::string brand[5] = { "Toyota", "Fiat", "Opel", "Honda", "NoBrand" };
	std::string model[11] = { "Yaris", "Corolla", "Aygo", "Punto", "Panda",
			"Astra", "Vectra", "Accord", "Civic", "Jazz", "NoModel" };
	std::cout << brand[this->carBrand] << " " << model[this->carModel];
}

Car::brand Car::returnBrand(std::string brand_) {
	if (brand_ == "Toyota")
		return Car::Toyota;
	else if (brand_ == "Fiat")
		return Car::Fiat;
	else if (brand_ == "Honda")
		return Car::Honda;
	else if (brand_ == "Opel")
		return Car::Opel;
	else
		return Car::NoBrand;
}

Car::model Car::returnModel(std::string model_) {
	if (model_ == "Yaris")
		return Car::Yaris;
	else if (model_ == "Corolla")
		return Car::Corolla;
	else if (model_ == "Aygo")
		return Car::Aygo;
	else if (model_ == "Punto")
		return Car::Punto;
	else if (model_ == "Astra")
		return Car::Astra;
	else if (model_ == "Vectra")
		return Car::Vectra;
	else if (model_ == "Accord")
		return Car::Accord;
	else if (model_ == "Civic")
		return Car::Civic;
	else if (model_ == "Jazz")
		return Car::Jazz;
	else if (model_ == "Panda")
		return Car::Panda;
	else
		return Car::NoModel;
}

std::string Car::printBrand() const {
	std::string brandsArray[5] = { "Toyota", "Fiat", "Opel", "Honda", "NoBrand" };
	return brandsArray[this->carBrand];
}

std::string Car::printModel() const {
	std::string modelsArray[11] = { "Yaris", "Corolla", "Aygo", "Punto",
			"Panda", "Astra", "Vectra", "Accord", "Civic", "Jazz", "NoModel" };
	return modelsArray[this->carModel];
}

